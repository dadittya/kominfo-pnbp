<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\SuperadminController;
use App\Http\Controllers\InstitutionController;
use App\Http\Controllers\TreasurerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Auth::routes();

//Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
	Route::get('map', function () {
		return view('pages.maps');
	})->name('map');
	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');
	Route::get('table-list', function () {
		return view('pages.tables');
	})->name('table');
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['prefix' => 'superadmin', 'middleware' => 'is_superadmin'], function () {
	Route::get('/',  [App\Http\Controllers\SuperadminController::class, 'index'])->name('superadmin.home');
	Route::get('/users', [App\Http\Controllers\Superadmin\UserController::class, 'index'])->name('superadmin.user.index');
	Route::get('/users/create', [App\Http\Controllers\Superadmin\UserController::class, 'create'])->name('superadmin.user.create');
});

Route::group(['prefix' => 'institution', 'middleware' => ['is_institution']], function () {
	Route::get('/',  [InstitutionController::class, 'index'])->name('institution.home');
	Route::get('/pnbp', [InstitutionController::class, 'index'])->name('institution.pnbp.index');
	Route::get('/pnbp/create', [InstitutionController::class, 'create'])->name('institution.pnbp.create');
	Route::post('/pnbp/create', [InstitutionController::class, 'store'])->name('institution.pnbp.store');
	Route::get('/pnbp/{id}/detail', [InstitutionController::class, 'detail'])->name('institution.pnbp.detail');
	Route::post('/pnbp/{id}/update/paid', [InstitutionController::class, 'PaidInstitutionFileUpdate'])->name('institution.pnbp.update.paid');
	Route::post('/pnbp/{id}/update/financial', [InstitutionController::class, 'FinancialStatementslUpdate'])->name('institution.pnbp.update.financial');
	Route::post('/pnbp/{id}/update/underpayment', [InstitutionController::class, 'UnderpaymentPaidInstitutionFileUpdate'])->name('institution.pnbp.update.underpayment');
});

Route::group(['prefix' => 'admin', 'middleware' => ['is_admin']], function () {
	Route::get('/',  [AdminController::class, 'index'])->name('admin.home');
	Route::get('/pnbp', [AdminController::class, 'index'])->name('admin.pnbp.index');
	//Route::get('/pnbp/create', [InstitutionController::class, 'create'])->name('admin.pnbp.create');
	//Route::post('/pnbp/create', [InstitutionController::class, 'store'])->name('admin.pnbp.store');
	Route::get('/pnbp/{id}/detail', [AdminController::class, 'detail'])->name('admin.pnbp.detail');
	Route::post('/pnbp/{id}/update', [AdminController::class, 'updateDueDate'])->name('admin.pnbp.update.duedate');
});

Route::group(['prefix' => 'treasurer', 'middleware' => ['is_admin']], function () {
	Route::get('/',  [TreasurerController::class, 'index'])->name('treasurer.home');
	Route::get('/pnbp', [TreasurerController::class, 'index'])->name('treasurer.pnbp.index');
	//Route::get('/pnbp/create', [InstitutionController::class, 'create'])->name('admin.pnbp.create');
	//Route::post('/pnbp/create', [InstitutionController::class, 'store'])->name('admin.pnbp.store');
	Route::get('/pnbp/{id}/detail', [TreasurerController::class, 'detail'])->name('treasurer.pnbp.detail');
	Route::post('/pnbp/{id}/update/paid/country', [TreasurerController::class, 'PaidCountryFileUpdate'])->name('treasurer.pnbp.update.paid.country');
	Route::post('/pnbp/{id}/update/underpayment/country', [TreasurerController::class, 'UnderpaymentPaidCountryFileUpdate'])->name('treasurer.pnbp.update.underpayment.country');
	Route::get('/pnpb/{filename}/download', [TreasurerController::class, 'download'])->name('treasurer.download');
});
