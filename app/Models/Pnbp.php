<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use stdClass;
use Illuminate\Support\Str;
use Illuminate\Http\Testing\MimeType;

use Illuminate\Support\Facades\Config;

class Pnbp extends Model
{
	use HasFactory;

	protected $dates = [
		'created_at', 'updated_at', 'due_date', 'paid_institutions_date', 'paid_country_date',
		'financial_statements_date', 'underpayment_date', 'underpayment_paid_institutions_date',
		'underpayment_paid_country_date', 'finish_date'
	];
	protected $fillable = [
		'year', 'total_amount', 'due_date', 'paid_amount', 'paid_institutions_date', 'paid_institutions_file',
		'paid_country_date', 'paid_country_file', 'financial_statements_file', 'underpayment_file', 'underpayment_paid_institutions_file',
		'underpayment_paid_country_file', 'financial_statements_date', 'underpayment_date', 'underpayment_paid_institutions_date',
		'underpayment_paid_country_date', 'finish_date', 'user_id', 'institution_id'
	];

	public function getPaidInstitutionsFilePath()
	{
		$value = $this->paid_institutions_file;
		if (!$value)
			return null;

		$path = Config::get('storage.paid_institution');
		return "{$path}/{$value}";
	}

	static public function random($extension = null)
	{
		$path = Config::get('storage.paid_institution');
		//if (!$path)
		//throw Exception("Config not found [{$key}]");
		$ret = new StdClass();
		$try = 250;
		do {
			//if ($try <= 0)
			//throw Exception("Storage::random fails to produce randomized filename");
			$hash = Str::random(32);
			if ($extension)
				$hash = $hash . '.' . $extension;
			$file = $path . '/' . $hash;
			$try -= 1;
		} while (file_exists($file));
		$ret = array($path, $hash, $file);
		return $ret;
	}

	public function setPaidInstitutionsFileAttribute($value)
	{
		$this->attributes['paid_institutions_file'] = null;
		if (!$value->isValid())
			return;

		//$mime = $value->getMimeType();
		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);

		//var_dump($file_path);die();
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['paid_institutions_file'] = $file_path[1];
		$this->attributes['paid_institutions_date'] = now();
		$this->attributes['status'] = 'paid_institutions';
		//$this->attributes['mimetype'] = $mime;
	}

	public function setPaidCountryFileAttribute($value)
	{
		$this->attributes['paid_country_file'] = null;
		if (!$value->isValid())
			return;

		//$mime = $value->getMimeType();
		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);

		//var_dump($file_path);die();
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['paid_country_file'] = $file_path[1];
		$this->attributes['paid_country_date'] = now();
		$this->attributes['status'] = 'paid_country';
		//$this->attributes['mimetype'] = $mime;
	}

	public function setFinancialStatementsFileAttribute($value)
	{
		$this->attributes['financial_statements_file'] = null;
		if (!$value->isValid())
			return;

		//$mime = $value->getMimeType();
		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);

		//var_dump($file_path);die();
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['financial_statements_file'] = $file_path[1];
		$this->attributes['financial_statements_file'] = now();
		$this->attributes['status'] = 'financial_statements';
		//$this->attributes['mimetype'] = $mime;
	}

	public function setUnderpaymentPaidInstitutionsFileAttribute($value)
	{
		$this->attributes['underpayment_paid_institutions_file'] = null;
		if (!$value->isValid())
			return;

		//$mime = $value->getMimeType();
		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);

		//var_dump($file_path);die();
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['underpayment_paid_institutions_file'] = $file_path[1];
		$this->attributes['underpayment_paid_institutions_date'] = now();
		$this->attributes['status'] = 'underpayment_paid_institutions';
		//$this->attributes['mimetype'] = $mime;
	}

	public function setUnderpaymentPaidCountryFileAttribute($value)
	{
		$this->attributes['underpayment_paid_country_file'] = null;
		if (!$value->isValid())
			return;

		//$mime = $value->getMimeType();
		$ext = $value->getClientOriginalExtension();

		$file_path = static::random($ext);

		//var_dump($file_path);die();
		$value->move($file_path[0], $file_path[1]);

		$this->attributes['underpayment_paid_country_file'] = $file_path[1];
		$this->attributes['underpayment_paid_country_date'] = now();
		$this->attributes['status'] = 'finish';
		//$this->attributes['mimetype'] = $mime;
	}

	public function getImageFullPath()
	{
		$value = $this->image;
		if ($value == null)
			return null;

		$path = Config::get('storage.jumbotron');
		return "{$path}/{$value}";
	}
}
