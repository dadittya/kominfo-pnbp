<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLevels extends Model
{
    use HasFactory;

    public function users()
    {
        return $this->hasMany(User::class, 'user_level_id');
    }
}
