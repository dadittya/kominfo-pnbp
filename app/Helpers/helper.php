<?php

function getStatus($str)
{
    switch ($str) {
        case 'submitted':
            return 'Terkirim';
            break;
        case 'paid_institutions':
            return 'Dibayar';
            break;
        case 'paid_country':
            return 'Disetor Negara';
            break;
        case 'financial_statements':
            return 'Laporan Keuangan';
            break;
        case 'underpayment':
            return 'Terdapat Kurang Bayar';
            break;
        case 'underpayment_paid_institutions':
            return 'Kurang Bayar Terbayar';
            break;
        case 'finish':
            return 'Selesai';
            break;
    }
}
