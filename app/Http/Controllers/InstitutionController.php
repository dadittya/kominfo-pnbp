<?php

namespace App\Http\Controllers;

use App\Models\Pnbp;
use Illuminate\Http\Request;
use App\Http\Requests\Institution\PnbpStoreRequest;
use App\Http\Requests\Institution\PaidInstitutionFileUpdateRequest;
use App\Http\Requests\Institution\FinancialStatementsUpdateRequest;
use App\Http\Requests\Institution\UnderpaymentPaidInstitutionFileUpdateRequest;

class InstitutionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //var_dump('foo bar');die();
        $pnbps = Pnbp::orderBy('id', 'desc')
            ->get();
        // return view('institution.pnbp.index')
        //     ->with('pnbps', $pnbps);
        return view('institution.pnbp.index2')
            ->with('pnbps', $pnbps);
    }

    public function create()
    {
        return view('institution.pnbp.create2');
    }

    public function store(PnbpStoreRequest $request)
    {
        $totalAmount = $request->total_amount;
        $paidAmount = $totalAmount * (5 / 100);
        $pnbp = new Pnbp();
        $pnbp->fill([
            'year' => $request->year,
            'total_amount' => $totalAmount,
            'paid_amount' => $paidAmount,
            'status' => 'submitted',
            'user_id' => auth()->user()->id,
            'institution_id' => auth()->user()->institution_id
        ]);

        $pnbp->save();

        return redirect()
            ->route('institution.pnbp.index')
            ->withStatus(__('Data Berhasil Disimpan'));
    }

    public function detail($id)
    {
        $pnbp = Pnbp::find($id);

        return view('institution.pnbp.detail2')
            ->with('pnbp', $pnbp);
    }

    public function PaidInstitutionFileUpdate($id, PaidInstitutionFileUpdateRequest $request)
    {
        $pnbp = Pnbp::find($id);

        $pnbp->paid_institutions_file = $request->paid_institution_file;
        $pnbp->save();

        return redirect()
            ->route('institution.pnbp.index')
            ->withStatus(__('Data Berhasil Disimpan'));
    }

    public function FinancialStatementslUpdate($id, FinancialStatementsUpdateRequest $request)
    {
        $pnbp = Pnbp::find($id);

        $pnbp->financial_statements_file = $request->financial_statements_file;
        $pnbp->save();

        return redirect()
            ->route('institution.pnbp.index')
            ->withStatus(__('Data Berhasil Disimpan'));
    }

    public function UnderpaymentPaidInstitutionFileUpdate($id, UnderpaymentPaidInstitutionFileUpdateRequest $request)
    {
        $pnbp = Pnbp::find($id);

        $pnbp->underpayment_paid_institutions_file = $request->underpayment_paid_institution_file;
        $pnbp->save();

        return redirect()
            ->route('institution.pnbp.index')
            ->withStatus(__('Data Berhasil Disimpan'));
    }
}
