<?php

namespace App\Http\Controllers;

use App\Models\Pnbp;
use Illuminate\Http\Request;
use App\Http\Requests\Institution\PnbpStoreRequest;
use App\Http\Requests\Admin\DueDateUpdateRequest;
use Illuminate\Support\Carbon;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //var_dump('foo bar');die();
        $pnbps = Pnbp::orderBy('id', 'desc')
            ->get();
        return view('admin.pnbp.index2')
            ->with('pnbps', $pnbps);
    }

    public function detail($id)
    {
        $pnbp = Pnbp::find($id);

        return view('admin.pnbp.detail2')
            ->with('pnbp', $pnbp);
    }

    public function updateDueDate($id, DueDateUpdateRequest $request)
    {
        $date = Carbon::createFromFormat('d/m/Y', $request->due_date);
        $pnbp = Pnbp::find($id);

        $pnbp->due_date = $date;
        if ($request->underpayment_amount) {
            # code...
            $pnbp->underpayment_amount = $request->underpayment_amount;
            $pnbp->status = 'Underpayment';
        }
        $pnbp->save();

        return redirect()->route('admin.pnbp.index');
    }
}
