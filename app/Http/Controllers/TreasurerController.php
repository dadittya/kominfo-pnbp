<?php

namespace App\Http\Controllers;

use App\Models\Pnbp;
use Illuminate\Http\Request;
use App\Http\Requests\Treasurer\PaidCountryFileUpdate;
use App\Http\Requests\Treasurer\PaidCountryFileUpdateRequest;
use App\Http\Requests\Treasurer\UnderpaymentPaidCountryFileUpdateRequest;
use Illuminate\Support\Facades\Config;

class TreasurerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //var_dump('foo bar');die();
        $pnbps = Pnbp::orderBy('id', 'desc')
            ->get();
        return view('treasurer.pnbp.index2')
            ->with('pnbps', $pnbps);
    }

    public function create()
    {
        return view('institution.pnbp.create');
    }

    public function detail($id)
    {
        $pnbp = Pnbp::find($id);

        return view('treasurer.pnbp.detail2')
            ->with('pnbp', $pnbp);
    }

    public function PaidCountryFileUpdate($id, PaidCountryFileUpdateRequest $request)
    {
        $pnbp = Pnbp::find($id);

        $pnbp->paid_country_file = $request->paid_country_file;
        $pnbp->save();

        return redirect()
            ->route('treasurer.pnbp.index')
            ->withStatus(__('Data Berhasil Disimpan'));
    }

    public function UnderpaymentPaidCountryFileUpdate($id, UnderpaymentPaidCountryFileUpdateRequest $request)
    {
        $pnbp = Pnbp::find($id);

        $pnbp->underpayment_paid_country_file = $request->underpayment_paid_country_file;
        $pnbp->save();

        return redirect()
            ->route('treasurer.pnbp.index')
            ->withStatus(__('Data Berhasil Disimpan'));
    }

    public function download($filename)
    {
        $path = Config::get('storage.paid_institution');
        $fullPath = "{$path}/{$filename}";

        $resp = response()->download($fullPath);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        return $resp;
    }
}
