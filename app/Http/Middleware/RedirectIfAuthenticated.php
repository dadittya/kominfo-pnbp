<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {

            if (Auth::guard($guard)->check()) {
                if (auth()->user()->user_level_id == 1) {
                    return redirect()->route('superadmin.home');
                }
                else if (auth()->user()->user_level_id == 2) {
                    return redirect()->route('institution.home');
                }
                else if (auth()->user()->user_level_id == 3) {
                    return redirect()->route('admin.home');
                }
                else if (auth()->user()->user_level_id == 4) {
                    return redirect()->route('treasurer.home');
                }
                else{
                    return redirect()->route('login');
                }
                // }
                //     return redirect(RouteServiceProvider::HOME);
                // }
            }
        }

        return $next($request);
    }
}
