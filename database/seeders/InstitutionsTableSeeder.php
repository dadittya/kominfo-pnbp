<?php

namespace Database\Seeders;

use App\Models\Institutions;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class InstitutionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $InstitutionList = $this->getInstitutionList();

        foreach( $InstitutionList as $institution )
        {
            (new Institutions())
                ->updateOrCreate($institution);
        }
    }

    private function getInstitutionList()
    {
        return [
            ['name' => 'PANDI',],
        ];
    }
}
