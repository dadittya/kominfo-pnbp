<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\UserLevels;
use App\Models\Institutions;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superdmin = UserLevels::where('name', 'superadmin')->first();
        $superdmin = $superdmin->id;

        $institution = UserLevels::where('name', 'institution')->first();
        $institution = $institution->id;

        $pnpbAdmin = UserLevels::where('name', 'pnpb_admin')->first();
        $pnpbAdmin = $pnpbAdmin->id;

        $treasurer = UserLevels::where('name', 'treasurer')->first();
        $treasurer = $treasurer->id;

        $pandi = Institutions::first();
        $pandi = $pandi->id;

        (new User())
        ->updateOrCreate([
            'email' => 'superadmin@example.com',
        ],[
            'name' => 'Superadmin',
            'email' => 'superadmin@example.com',
            'email_verified_at' => now(),
            'password' => Hash::make('Password1234'),
            'user_level_id' => $superdmin
        ]);

        (new User())
            ->updateOrCreate([
                'email' => 'pnpb_admin@example.com',
            ],[
                'name' => 'PNPB Admin',
                'email' => 'pnpb_admin@example.com',
                'email_verified_at' => now(),
                'password' => Hash::make('Password1234'),
                'user_level_id' => $pnpbAdmin
            ]);

        (new User())
        ->updateOrCreate([
            'email' => 'bendahara@example.com',
            ],[
            'name' => 'Bendahara Aptika',
            'email' => 'bendahara@example.com',
            'email_verified_at' => now(),
            'password' => Hash::make('Password1234'),
            'user_level_id' => $treasurer
        ]);

        (new User())
        ->updateOrCreate([
            'email' => 'pandi@example.com',
            ],[
            'name' => 'Pandi',
            'email' => 'pandi@example.com',
            'email_verified_at' => now(),
            'password' => Hash::make('Password1234'),
            'user_level_id' => $institution,
            'institution_id' => $pandi
        ]);
    }
    
}
