<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

use App\Models\Pnbp;
use App\Models\User;

class PnbpTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'pandi@example.com')
                        ->first();
        
        DB::table('pnbps')->insert([
            'total_amount' => 100000000,
            'paid_amount' => 5000000,
            'status' => 'submitted',
            'created_at' => now(),
            'updated_at' => now(),
            'user_id' => $user->id,
            'institution_id' => $user->institution_id,
            'year' => '2021'
        ]);

        $pnpb = Pnbp::first();

        DB::table('pnbp_items')->insert([
            'pnbp_id' => $pnpb->id,
            'name' => "item 1",
            'amount' => '25000000',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('pnbp_items')->insert([
            'pnbp_id' => $pnpb->id,
            'name' => "item 2",
            'amount' => '30000000',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('pnbp_items')->insert([
            'pnbp_id' => $pnpb->id,
            'name' => "item 3",
            'amount' => '15000000',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('pnbp_items')->insert([
            'pnbp_id' => $pnpb->id,
            'name' => "item 4",
            'amount' => '30000000',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
