<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([InstitutionsTableSeeder::class]);
        $this->call([UserLevelsTableSeeder::class]);
        $this->call([UsersTableSeeder::class]);
        //$this->call([PnbpTableSeeder::class]);
    }
}
