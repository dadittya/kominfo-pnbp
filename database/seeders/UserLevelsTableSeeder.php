<?php

namespace Database\Seeders;

use App\Models\UserLevels;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UserLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userLevelList = $this->getUserLevelList();

        foreach( $userLevelList as $userLevel )
        {
            (new UserLevels())
                ->updateOrCreate($userLevel);
        }

    }

    private function getUserLevelList()
    {
        return [
            ['name' => 'superadmin',],
            ['name' => 'institution',],
            ['name' => 'pnpb_admin',],
            ['name' => 'treasurer',],
        ];
    }

}
