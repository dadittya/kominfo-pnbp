<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePpnbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pnbps', function (Blueprint $table) {
            $table->id();
            $table->decimal('total_amount', 15, 2);
            $table->decimal('paid_amount', 15, 2);
            $table->string('year');
            $table->date('due_date')->nullable();
            $table->enum('status', ['submitted', 'paid_institutions', 'paid_country', 'financial_statements', 'underpayment', 'underpayment_paid_institutions', 'finish']);
            $table->datetime('paid_institutions_date')->nullable();
            $table->string('paid_institutions_file')->nullable();
            $table->datetime('paid_country_date')->nullable();
            $table->string('paid_country_file')->nullable();
            $table->datetime('financial_statements_date')->nullable();
            $table->string('financial_statements_file')->nullable();
            $table->datetime('underpayment_date')->nullable();
            $table->string('underpayment_file')->nullable();
            $table->datetime('underpayment_paid_institutions_date')->nullable();
            $table->string('underpayment_paid_institutions_file')->nullable();
            $table->datetime('underpayment_paid_country_date')->nullable();
            $table->string('underpayment_paid_country_file')->nullable();
            $table->datetime('finish_date')->nullable();
            $table->timestamps();

            $table->foreignId('user_id')
            ->references('id')
            ->on('users');

            $table->foreignId('institution_id')
            ->references('id')
            ->on('institutions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ppnbs');
    }
}
