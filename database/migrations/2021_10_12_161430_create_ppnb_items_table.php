<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePpnbItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pnbp_items', function (Blueprint $table) {
            $table->id();
            //$table->integer('pnbp_id')->unsigned();
            $table->string('name');
            $table->decimal('amount', 15, 2);
            $table->timestamps();

            $table->foreignId('pnbp_id')
        		->references('id')
        		->on('pnbps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ppnb_items');
    }
}
