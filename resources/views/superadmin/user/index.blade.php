@extends('superadmin.layout')

@section('content')
<div class="row">
    <div class="col">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">Users</h3>
                    </div>
                    <div class="col-4 text-right">
                        <a href="{{ route('superadmin.user.create') }}" class="btn btn-sm btn-primary">Add user</a>
                    </div>
                </div>
            </div>
            
            <div class="col-12">
        </div>

        <div class="table-responsive">
            <table class="table table-flush">
                <thead class="thead-light">
                    <tr>
                        <th scope="col" class="sort">Nama</th>
                        <th scope="col">Email</th>
                        <th scope="col">Level</th>
                        <th scope="col">Dibuat</th>
                        <th scope="col">Diubah</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $key => $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>
                            <a href="mailto:admin@argon.com">{{ $user->email }}</a>
                        </td>
                        <td></td>
                        <td></td>   
                        <td class="text-right">
                            <div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                                                            <a class="dropdown-item" href="">Edit</a>
                                                                                    </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer py-4">
            <nav class="d-flex justify-content-end" aria-label="...">
                
            </nav>
        </div>
    </div>
@endsection