@extends('admin.layout2')

@section('active')
active
@endsection
@section('content')
    
<section class="section">
    <div class="row" id="table-head">
        <div class="col-12">
            <div class="card">
                <div class="card-content p-3">
                    <!-- table head dark -->
                    <div class="table-responsive">
                        <table class="table mb-0 p-4">
                            <thead class="thead-dark">
                                <tr>
                                    <th>TAHUN</th>
                                    <th>TANGGAL</th>
                                    <th>JUMLAH</th>
                                    <th>JUMLAH BAYAR</th>
                                    <th>JATUH TEMPO</th>
                                    <th>STATUS</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($pnbps as $key => $pnbp)
                                <tr>
                                    <td>
                                        {{ $pnbp->year }}
                                    </td>
                                    <td>
                                        {{ $pnbp->created_at->format('d-m-Y') }}
                                    </td>
                                    <td>
                                        {{ number_format($pnbp->total_amount, 0, ',', '.') }}
                                    </td>
                                    <td>
                                        {{ number_format($pnbp->paid_amount, 0, ',', '.') }}
                                    </td>
                                    <td>
                                        {{ $pnbp->due_date != '' ? $pnbp->due_date->format('d-m-Y') : '-' }}
                                    </td>
                                    <td>
                                        {{ getStatus($pnbp->status) }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.pnbp.detail', $pnbp->id)}}" class="btn btn-sm btn-primary">Detail</a>
                                    </td>
                                    
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection