@extends('admin.layout2')

@section('active')
active
@endsection
@section('content')
    
<section class="section">
    <div class="row">
        <div class="col-xl order-xl-1">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <h3 class="mb-0">{{ __('Tambah Data') }}</h3>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.pnbp.update.duedate', $pnbp->id)}}" autocomplete="off">
                        @csrf

                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif


                        <div class="pl-lg-4">
                            <div class="form-group{{ $errors->has('year') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-year">{{ __('Tahun') }}</label>
                                <input type="text" name="year" id="input-year" class="form-control form-control-alternative{{ $errors->has('year') ? ' is-invalid' : '' }}" placeholder="{{ __('Tahun') }}" value="{{ $pnbp->year }}" readonly autofocus>

                                @if ($errors->has('year'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('year') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('total_amount') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-total-amount">{{ __('Jumlah') }}</label>
                                <input type="text" name="total_amount" id="input-total-amount" class="form-control form-control-alternative{{ $errors->has('total_amount') ? ' is-invalid' : '' }}" placeholder="{{ __('Jumlah') }}" value="{{ $pnbp->total_amount }}" readonly>

                                @if ($errors->has('total_amount'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('total_amount') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('due_date') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-due-date">{{ __('Jatuh Tempo') }}</label>
                                <input type="text" name="due_date" id="input-due-date" class="form-control datepicker form-control-alternative{{ $errors->has('due_date') ? ' is-invalid' : '' }}" placeholder="{{ __('DD/MM/YYYY') }}" 
                                    value="{{ $pnbp->due_date != '' ? $pnbp->due_date->format('d/m/Y') : date('d/m/Y') }}">

                                @if ($errors->has('due_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('due_date') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('due_date') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-due-date">{{ __('Kurang Bayar') }}</label>
                                <input type="number" name="underpayment_amount" id="input-underpayment-amount" class="form-control form-control-alternative{{ $errors->has('underpayment_amount') ? ' is-invalid' : '' }}" placeholder="{{ __('Jumlah Kurang Bayar') }}" min="0">

                                @if ($errors->has('due_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('due_date') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">{{ __('Simpan') }}</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection