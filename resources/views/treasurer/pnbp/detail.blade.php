@extends('institution.layout')
@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                    </div>
                    <div class="card-body">
                        @if ($pnbp->paid_country_file == '')
                            <form method="post" action="{{ route('treasurer.pnbp.update.paid.country', $pnbp->id)}}" autocomplete="off" enctype="multipart/form-data">
                        @elseif($pnbp->underpayment_paid_country_file == '')
                            <form method="post" action="{{ route('treasurer.pnbp.update.underpayment.country', $pnbp->id)}}" autocomplete="off" enctype="multipart/form-data">
                        @else
                        <form method="post" action="" autocomplete="off">
                        @endif
                            @csrf
                            
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('year') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-year">{{ __('Tahun') }}</label>
                                    <input type="text" name="year" id="input-year" class="form-control form-control-alternative{{ $errors->has('year') ? ' is-invalid' : '' }}" placeholder="{{ __('Tahun') }}" value="{{ $pnbp->year }}" readonly autofocus>

                                    @if ($errors->has('year'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('year') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('total_amount') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-total-amount">{{ __('Jumlah') }}</label>
                                    <input type="text" name="total_amount" id="input-total-amount" class="form-control form-control-alternative{{ $errors->has('total_amount') ? ' is-invalid' : '' }}" placeholder="{{ __('Jumlah') }}" value="{{ $pnbp->total_amount }}" readonly>

                                    @if ($errors->has('total_amount'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('total_amount') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('due_date') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-due-date">{{ __('Jatuh Tempo') }}</label>
                                    <input type="text" name="due_date" id="input-due-date" class="form-control form-control-alternative{{ $errors->has('due_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Jatuh Tempo') }}" value="{{ $pnbp->due_date != '' ? $pnbp->due_date->format('d-m-Y') : '-' }}" readonly>
                                </div>

                                @if ($pnbp->paid_institutions_file != '')

                                    <div class="form-group">
                                        <label class="form-control-label"> {{ __('Tanggal Pembayaran') }}</label>
                                        <input type="text" name="due_date" id="input-due-date" class="form-control form-control-alternative{{ $errors->has('due_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Jatuh Tempo') }}" value="{{ $pnbp->paid_institutions_date != '' ? $pnbp->paid_institutions_date->format('d-m-Y') : '-' }}" readonly>
                                    </div>

                                    <div class="form-group">
                                        <a href="{{ route('treasurer.download', $pnbp->paid_institutions_file)}}" target="_blank" class="btn btn-sm btn-primary">Download Bukti Bayar</a>
                                    </div>

                                    <div class="form-group{{ $errors->has('paid_country_file') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-paid-country-file">{{ __('Bukti Setor Negara') }}</label>
                                        <input type="file" name="paid_country_file" id="input-paid-country-file" class="form-control form-control-alternative{{ $errors->has('paid_country_file') ? ' is-invalid' : '' }}" placeholder="{{ __('') }}" value="">
    
                                        @if ($errors->has('paid_country_file'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('paid_country_file') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    @if ($pnbp->underpayment_paid_institutions_date != '' && $pnbp->underpayment_paid_country_date == '')
                                        <div class="form-group{{ $errors->has('underpayment_paid_country_file') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-underpayment-paid-country-file">{{ __('Bukti Setor Negara Pembayaran Kurang Bayar') }}</label>
                                            <input type="file" name="underpayment_paid_country_file" id="input-underpayment-paid-country-file" class="form-control form-control-alternative{{ $errors->has('underpayment_paid_country_file') ? ' is-invalid' : '' }}" placeholder="{{ __('') }}" value="">
        
                                            @if ($errors->has('underpayment_paid_country_file'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('underpayment_paid_country_file') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    @endif


                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success mt-4">{{ __('Simpan') }}</button>
                                    </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
@endsection