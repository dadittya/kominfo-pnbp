@extends('treasurer.layout')

@section('content')
<div class="row">
    <div class="col">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col-8">
                    </div>
                </div>
            </div>
            
            <div class="col-12">
        </div>

        <div class="table-responsive">
            <table class="table table-flush">
                <thead class="thead-light">
                    <tr style="text-align: left;">
                        <th scope="col">Tahun</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Jumlah</th>
                        <th scope="col">Jumlah Bayar</th>
                        <th scope="col">Jatuh Tempo</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pnbps as $key => $pnbp)
                    <tr>
                        <td>
                            {{ $pnbp->year }}
                        </td>
                        <td>
                            {{ $pnbp->created_at->format('d-m-Y') }}
                        </td>
                        <td>
                            {{ number_format($pnbp->total_amount, 0, ',', '.') }}
                        </td>
                        <td>
                            {{ number_format($pnbp->paid_amount, 0, ',', '.') }}
                        </td>
                        <td>
                            {{ $pnbp->due_date != '' ? $pnbp->due_date->format('d-m-Y') : '-' }}
                        </td>
                        <td>
                            {{ getStatus($pnbp->status) }}
                        </td>
                        <td>
                            <a href="{{ route('treasurer.pnbp.detail', $pnbp->id)}}" class="btn btn-sm btn-primary">Detail</a>
                        </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer py-4">
            <nav class="d-flex justify-content-end" aria-label="...">
                
            </nav>
        </div>
    </div>
@endsection