@extends('institution.layout2')

@section('active')
active
@endsection
@section('content')
    
<section class="section">
    <div class="row">
        <div class="col-xl order-xl-1">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <h3 class="mb-0">{{ __('Detail') }}</h3>
                    </div>
                </div>
                <div class="card-body">
                    @if ($pnbp->paid_country_date != '')
                        @if ($pnbp->underpayment_amount > 0)
                            <form method="post" action="{{ route('institution.pnbp.update.underpayment', $pnbp->id)}}" autocomplete="off" enctype="multipart/form-data">
                        @else 
                            <form method="post" action="{{ route('institution.pnbp.update.financial', $pnbp->id)}}" autocomplete="off" enctype="multipart/form-data">
                        @endif
                    @elseif ($pnbp->due_date != '')
                        <form method="post" action="{{ route('institution.pnbp.update.paid', $pnbp->id)}}" autocomplete="off" enctype="multipart/form-data">
                    
                        <form method="post" action="" autocomplete="off" enctype="multipart/form-data">
                    @endif
                        @csrf
                        
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif


                        <div class="pl-lg-4">
                            <div class="form-group{{ $errors->has('year') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-year">{{ __('Tahun') }}</label>
                                <input type="text" name="year" id="input-year" class="form-control form-control-alternative{{ $errors->has('year') ? ' is-invalid' : '' }}" placeholder="{{ __('Tahun') }}" value="{{ $pnbp->year }}" readonly autofocus>

                                @if ($errors->has('year'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('year') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('total_amount') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-total-amount">{{ __('Jumlah') }}</label>
                                <input type="text" name="total_amount" id="input-total-amount" class="form-control form-control-alternative{{ $errors->has('total_amount') ? ' is-invalid' : '' }}" placeholder="{{ __('Jumlah') }}" value="{{ $pnbp->total_amount }}" readonly>

                                @if ($errors->has('total_amount'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('total_amount') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('due_date') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-due-date">{{ __('Jatuh Tempo') }}</label>
                                <input type="text" name="due_date" id="input-due-date" class="form-control form-control-alternative{{ $errors->has('due_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Jatuh Tempo') }}" value="{{ $pnbp->due_date != '' ? $pnbp->due_date->format('d-m-Y') : '-' }}" readonly>
                            </div>

                            @if ($pnbp->due_date != '')

                                @if ($pnbp->paid_institutions_file != '')

                                    <div class="form-group">
                                        <label class="form-control-label"> {{ __('Tanggal Pembayaran') }}</label>
                                        <input type="text" name="due_date" id="input-due-date" class="form-control form-control-alternative{{ $errors->has('due_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Jatuh Tempo') }}" value="{{ $pnbp->paid_institutions_date != '' ? $pnbp->paid_institutions_date->format('d-m-Y') : '-' }}" readonly>
                                    </div>

                                    <div class="form-group">
                                        <a href="{{ route('treasurer.download', $pnbp->paid_institutions_file)}}" target="_blank" class="btn btn-sm btn-primary">Download Bukti Bayar</a>
                                    </div>

                                    @if ($pnbp->paid_country_file != '')

                                        <div class="form-group">
                                            <label class="form-control-label"> {{ __('Tanggal Setor Negara') }}</label>
                                            <input type="text" name="due_date" id="input-due-date" class="form-control form-control-alternative{{ $errors->has('due_date') ? ' is-invalid' : '' }}" placeholder="{{ __('Jatuh Tempo') }}" value="{{ $pnbp->paid_country_date != '' ? $pnbp->paid_country_date->format('d-m-Y') : '-' }}" readonly>
                                        </div>

                                        <div class="form-group">
                                            <a href="{{ route('treasurer.download', $pnbp->paid_country_file)}}" target="_blank" class="btn btn-sm btn-primary">Download Bukti Setor Negara</a>
                                        </div>

                                        <div class="form-group{{ $errors->has('financial_statements_file') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-financial_statements-file">{{ __('Laporan Keuangan') }}</label>
                                            <input type="file" name="financial_statements_file" id="financial-statements-file" class="form-control form-control-alternative{{ $errors->has('nput-financial-statements-file') ? ' is-invalid' : '' }}" placeholder="{{ __('Laporan Keuangan') }}" value="">

                                            @if ($errors->has('financial_statements_file'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('financial_statements_file') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                    @endif

                                    @if ($pnbp->underpayment_amount > 0)
                                        @if ($pnbp->underpayment_paid_country_date == '')    
                                            <div class="form-group{{ $errors->has('underpayment_amount') ? ' has-danger' : '' }}">
                                                <label class="form-control-label" for="input-underpayment-amount">{{ __('Jumlah Kurang Bayar') }}</label>
                                                <input type="text" name="underpayment_amount" id="input-underpayment-amount" class="form-control form-control-alternative{{ $errors->has('underpayment_amount') ? ' is-invalid' : '' }}" placeholder="{{ __('Jumlah') }}" value="{{ $pnbp->underpayment_amount }}" readonly>
            
                                                @if ($errors->has('underpayment_amount'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('underpayment_amount') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('underpayment_paid_institution_file') ? ' has-danger' : '' }}">
                                                <label class="form-control-label" for="input-paid-institution-file">{{ __('Bukti Pembayaran Kurang Bayar') }}</label>
                                                <input type="file" name="underpayment_paid_institution_file" id="input-underpayment-paid-institution-file" class="form-control form-control-alternative{{ $errors->has('underpayment_paid_institution_file') ? ' is-invalid' : '' }}" placeholder="{{ __('Bukti Pembayaran Kurang Bayar') }}" value="">

                                                @if ($errors->has('underpayment_paid_institution_file'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('underpayment_paid_institution_file') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        @endif
                                    @endif
                                @else
                                    <div class="form-group{{ $errors->has('paid_institution_file') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-paid-institution-file">{{ __('Bukti Pembayaran') }}</label>
                                        <input type="file" name="paid_institution_file" id="input-paid-institution-file" class="form-control form-control-alternative{{ $errors->has('paid_institution_file') ? ' is-invalid' : '' }}" placeholder="{{ __('Bukti Pembayaran') }}" value="">

                                        @if ($errors->has('paid_institution_file'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('paid_institution_file') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                @endif

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Simpan') }}</button>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection